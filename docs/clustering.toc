\contentsline {section}{\numberline {1}Scopo del documento}{2}{section.1}%
\contentsline {section}{\numberline {2}Introduzione}{2}{section.2}%
\contentsline {subsection}{\numberline {2.1}Clustering gerarchico}{2}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Clustering partizionale}{3}{subsection.2.2}%
\contentsline {subsection}{\numberline {2.3}Clustering density based}{3}{subsection.2.3}%
\contentsline {section}{\numberline {3}Dbscan}{3}{section.3}%
\contentsline {subsubsection}{\numberline {3.0.1}Descrizione}{3}{subsubsection.3.0.1}%
\contentsline {subsubsection}{\numberline {3.0.2}Complessità}{4}{subsubsection.3.0.2}%
\contentsline {section}{\numberline {4}K-means}{4}{section.4}%
\contentsline {subsubsection}{\numberline {4.0.1}Descrizione}{4}{subsubsection.4.0.1}%
\contentsline {subsubsection}{\numberline {4.0.2}Complessità}{5}{subsubsection.4.0.2}%
\contentsline {section}{\numberline {5}Dbscan vs K-means}{5}{section.5}%
\contentsline {subsection}{\numberline {5.1}Iris}{5}{subsection.5.1}%
