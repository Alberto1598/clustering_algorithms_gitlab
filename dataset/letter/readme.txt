
Original data downloaded from:
http://archive.ics.uci.edu/ml/datasets/Letter+Recognition

letter-recognition.data: Original data
letter.txt: letter-recognition.data converted to format readable by txt2cb and MATLAB
letter.ts: letter.txt converted to TS format
letter.pa: Class label partitions of data
letter_pi.txt: Mapping between integer class labels and original labels

letter4p1-4: Dataset divided into 4 parts in increasing overlap between classes.

