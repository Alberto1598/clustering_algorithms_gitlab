
from fpdf import FPDF
from PyPDF2 import PdfFileMerger
from weasyprint import HTML, CSS

def add_to_pdf(filename, width, height):
	file = open (filename, "r")
	for i in file :
		pdf.cell(width, height, txt = i, ln = 1)

def print_title(text):
	pdf.set_font("Arial", "BU", 15)
	pdf.cell(200, 5, txt = text, ln = 1, align = "C")
	pdf.set_font("Times", size=10)

pdf = FPDF()
pdf.add_page()
HTML("scalene-report.html").write_pdf("scalene-profiler.pdf",  stylesheets=[CSS(string='@page {size: 15in 10in;}')])
print_title ("CPROFILE REPORT")
add_to_pdf("cProfile-report.txt", 200, 5)
pdf.add_page()
print_title ("CPROFILE GRAPH")
pdf.image("Cprofile-graph.png", 0, 20, 300, 180)
pdf.add_page()
print_title("MEMORY PROFILER GRAPH")
pdf.image("memory-profiler.png", 0, 20, 220, 130)
pdf.add_page()
print_title ("PYLINT REPORT")
add_to_pdf("pylint-report.txt", 200, 5)
pdf.add_page()
print_title("BANDIT REPORT")
add_to_pdf("bandit-report.txt", 200, 5)
pdf.output("rep.pdf")
merger = PdfFileMerger()
merger.append("scalene-profiler.pdf")
merger.append("rep.pdf")
merger.write("report.pdf")
merger.close()





